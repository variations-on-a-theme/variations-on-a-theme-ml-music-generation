# ♫Variations on a Theme - ML Music Generation♫ 

## Using Magenta's MelodyRNN

-----


This repository holds a fork of [Magenta](https://github.com/magenta/magenta) as well as Blues melody midi data from [this Kaggle dataset](https://www.kaggle.com/datasets/function9/blues-genre-midi-melodies), and the generated melodies from the six training and evaluation runs using that data. A sample of the generated melodies can be found in the "generated_melodies" folder. The holds the originals as well as a couple of the melodies converted to .wav files using the synthesizer found in the partner project "♫Variations on a Theme - Midi Interpretation♫."

For an introduction to the topics of Recurrent Neural Networks (RNNs) and Long Short-Term Memory (LSTM), explore [The Unreasonable Effectiveness of Recurrent Neural Networks](https://karpathy.github.io/2015/05/21/rnn-effectiveness/) and [Understanding LSTM Networks](https://colah.github.io/posts/2015-08-Understanding-LSTMs/).

-----

https://soundcloud.com/amber-shore-600817638/hibernate-enemy

https://soundcloud.com/amber-shore-600817638/time-for-over
